# Alphanumeric blob to pixel converter

This project is an open-source tool that can be be used to perform steganography.

Feel free to improve it or use it in your own projects.

Each file is distributed "as is" in the hope that it will be useful, but without any warranty expressed or implied.

### Installation

⚠ This project has been developped on a Python3 environment.

```bash
pip install -r requirements.txt
```

### Usage

```
usage: alpha_to_pixel.py [-h] [-i INPUT] [-o OUTPUT]

Alphanumeric blob to pixel v0.1-dev

optional arguments:
  -h, --help            show this help message and exit
  -i INPUT, --input INPUT
                        path to alphanumeric text file
  -o OUTPUT, --output OUTPUT
                        path to output image
```
